package example.users.common;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import org.springframework.data.querydsl.QPageRequest;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class QueryDslSupport<E> extends QueryDslRepositorySupport {
    private final EntityPath<E> entity;

    public QueryDslSupport(Class<E> domainClass, EntityPath<E> entity) {
        super(domainClass);
        this.entity = entity;
    }

    protected E findOne(Predicate predicate) {
        JPQLQuery<E> query = from(entity).where(predicate);
        return query.fetchOne();
    }

    protected List<E> findAll(Predicate predicate, Map<EntityPath, Path> joins) {
        JPQLQuery query = from(entity).where(predicate);
        if (joins != null) {
            for (Map.Entry<EntityPath, Path> entry : joins.entrySet()) {
                query = query.innerJoin(entry.getKey(), entry.getValue()).fetchJoin();
            }
        }

        //noinspection unchecked
        return query.fetch();
    }

    protected Page<E> findAll(Predicate predicate) {
        return findAll(predicate, new QPageRequest(0, Integer.MAX_VALUE), null);
    }

    protected Page<E> findAll(Predicate predicate, Pageable pageable) {
        return findAll(predicate, pageable, null);
    }

    protected Page<E> findAll(Predicate predicate, Pageable pageable, Map<EntityPath, Path> joins) {
        if (pageable == null) {
            return findAll(predicate, new QPageRequest(0, Integer.MAX_VALUE), joins);
        }

        JPQLQuery<E> query = from(entity).where(predicate);

        long total = query.fetchCount();
        JPQLQuery pagedQuery = getQuerydsl().applyPagination(pageable, query);
        if (joins != null) {
            for (Map.Entry<EntityPath, Path> entry : joins.entrySet()) {
                pagedQuery = pagedQuery.innerJoin(entry.getKey(), entry.getValue()).fetchJoin();
            }
        }

        List<E> content = total > pageable.getOffset() ? pagedQuery.fetch() : Collections.emptyList();

        return new PageImpl<>(content, pageable, total);
    }
}
