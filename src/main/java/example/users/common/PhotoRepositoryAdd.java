package example.users.common;

import example.users.entity.Photo;

/**
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public interface PhotoRepositoryAdd extends MyRepository<Photo, Long> {
}
