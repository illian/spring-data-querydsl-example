package example.users.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;

import javax.persistence.EntityManager;
import java.io.Serializable;

@Slf4j
public class MyRepositoryImpl<T, ID extends Serializable> extends QueryDslJpaRepository<T, ID> implements MyRepository<T, ID> {
    public MyRepositoryImpl(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
    }

    public void sharedCustomMethod(ID id) {
        log.info("It's sharedCustomMethod with id = " + id);
    }
}
