package example.users.dialect;

import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.TextType;

/**
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class H2CustomDialect extends H2Dialect {
    public H2CustomDialect() {
        super();
        registerFunction("arr_length", new StandardSQLFunction("ARRAY_LENGTH"));
        registerFunction("arr_in", new StandardSQLFunction("ARRAY_CONTAINS", TextType.INSTANCE));
    }
}
