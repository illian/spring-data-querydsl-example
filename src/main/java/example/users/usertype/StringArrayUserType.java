package example.users.usertype;

public class StringArrayUserType extends ArrayUserType<String> {
    public StringArrayUserType() {
        super(String[].class);
    }
}
