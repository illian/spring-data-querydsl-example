package example.users.repository;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import example.users.common.QueryDslSupport;
import example.users.entity.QUser;
import example.users.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class UserRepositoryImpl extends QueryDslSupport<User> implements UserRepositoryCustom {
    public UserRepositoryImpl() {
        super(User.class, QUser.user);
    }

    @Override
    public Page<User> getPage(Predicate predicate, Pageable pageable, Map<EntityPath, Path> joins) {
        return findAll(getActiveUsers(predicate), pageable, joins);
    }

    private BooleanExpression getActiveUsers(Predicate predicate) {
        return QUser.user.isBlocked.eq(false).and(QUser.user.isDeleted.eq(false)).and(predicate);
    }
}
