/*
 * Copyright 2015-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example.users.repository;

import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.core.types.dsl.TemporalExpression;
import example.users.entity.QUser;
import example.users.entity.User;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository to manage {@link User}s. Also implements {@link QueryDslPredicateExecutor} to enable predicate filtering
 * on Spring MVC controllers as well as {@link QuerydslBinderCustomizer} to tweak the way predicates are created for
 * properties.
 *
 * @author Christoph Strobl
 * @author Oliver Gierke
 */
public interface UserRepository extends CrudRepository<User, Long>, QueryDslPredicateExecutor<User>,
        QuerydslBinderCustomizer<QUser>, UserRepositoryCustom {

    /*
     * (non-Javadoc)
     * @see org.springframework.data.querydsl.binding.QuerydslBinderCustomizer#customize(org.springframework.data.querydsl.binding.QuerydslBindings, com.mysema.query.types.EntityPath)
     */
    @Override
    default void customize(QuerydslBindings bindings, QUser root) {
        bindings.bind(QUser.user.created).first(TemporalExpression::after);
        bindings.bind(QUser.user.languages).first((path, strings) ->
                Expressions.booleanTemplate(String.format("true = arr_in(%s, {0})", path), strings[0]));

        bindings.bind(String.class).first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
        bindings.excluding(root.password);
    }
}
