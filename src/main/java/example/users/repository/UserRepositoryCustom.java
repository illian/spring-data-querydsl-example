package example.users.repository;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import example.users.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public interface UserRepositoryCustom {
    Page<User> getPage(Predicate predicate, Pageable pageable, Map<EntityPath, Path> joins);
}
