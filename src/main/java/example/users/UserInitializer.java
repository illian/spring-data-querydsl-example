/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example.users;

import example.users.entity.Address;
import example.users.entity.Photo;
import example.users.entity.User;
import example.users.repository.AddressRepository;
import example.users.repository.PhotoRepository;
import example.users.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.separator.DefaultRecordSeparatorPolicy;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.capitalize;

@RequiredArgsConstructor
public class UserInitializer {

    private final UserRepository userRepository;
    private final AddressRepository addressRepository;
    private final PhotoRepository photoRepository;

    @Transactional
    public void init() throws Exception {
        long count = userRepository.count();
        if (count == 0) {
            addressRepository.deleteAll();
            photoRepository.deleteAll();
            List<User> users = readUsers(new ClassPathResource("randomuser.me.csv"));
            for (User user : users) {
                Address address = user.getAddress();
                Photo photo = user.getPhoto();
                user.setAddress(null);
                user.setPhoto(null);
                userRepository.save(user);
                addressRepository.save(address);
                photoRepository.save(photo);
            }
        }
    }

    private List<User> readUsers(Resource resource) throws Exception {
        List<String> languages = new ArrayList<>();
        languages.add("english");
        languages.add("german");
        languages.add("french");
        languages.add("russian");
        languages.add("chinese");
        languages.add("japanese");
        languages.add("swedish");

        Random random = new Random(1L);
        Scanner scanner = new Scanner(resource.getInputStream());
        String line = scanner.nextLine();
        scanner.close();

        FlatFileItemReader<User> reader = new FlatFileItemReader<>();
        reader.setResource(resource);

        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setNames(line.split(","));
        tokenizer.setStrict(false);

        DefaultLineMapper<User> lineMapper = new DefaultLineMapper<>();
        lineMapper.setFieldSetMapper(fields -> {
            User user = new User();
            user.setCreated(new Date());

            user.setEmail(fields.readString("email"));
            user.setFirstname(capitalize(fields.readString("first")));
            user.setLastname(capitalize(fields.readString("last")));
            user.setNationality(fields.readString("nationality"));

            String city = Arrays.stream(fields.readString("city").split(" "))//
                    .map(StringUtils::capitalize)//
                    .collect(Collectors.joining(" "));
            String street = Arrays.stream(fields.readString("street").split(" "))//
                    .map(StringUtils::capitalize)//
                    .collect(Collectors.joining(" "));

            try {
                user.setAddress(new Address(city, street, fields.readString("zip"), user));
            } catch (IllegalArgumentException e) {
                user.setAddress(new Address(city, street, fields.readString("postcode"), user));
            }

            user.setPhoto(new Photo(fields.readString("large"), fields.readString("medium"),
                    fields.readString("thumbnail"), user));

            user.setUsername(fields.readString("username"));
            user.setPassword(fields.readString("password"));

            if (random.nextInt(20) == 0) {
                user.setIsDeleted(true);
            } else {
                user.setIsDeleted(false);
            }

            if (random.nextInt(20) == 0) {
                user.setIsBlocked(true);
            } else {
                user.setIsBlocked(false);
            }

            user.setLanguages(getRandomLanguagesArray(languages, random));

            return user;
        });

        lineMapper.setLineTokenizer(tokenizer);

        reader.setLineMapper(lineMapper);
        reader.setRecordSeparatorPolicy(new DefaultRecordSeparatorPolicy());
        reader.setLinesToSkip(1);
        reader.open(new ExecutionContext());

        List<User> users = new ArrayList<>();
        User user;

        do {
            user = reader.read();

            if (user != null) {
                users.add(user);
            }

        } while (user != null);

        return users;
    }

    private String[] getRandomLanguagesArray(List<String> languageList, Random random) {
        int size = random.nextInt(2) + 1;
        String[] result = new String[size];
        for (int i = 0; i < size; i++) {
            result[i] = languageList.get(random.nextInt(languageList.size() - 1));
        }

        return result;
    }
}
