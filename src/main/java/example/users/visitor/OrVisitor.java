package example.users.visitor;

import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.Expressions;

/**
 * This visitor change common AND expressions in query to OR.
 *
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class OrVisitor implements Visitor<Predicate, Expression> {
    @Override
    public Predicate visit(Constant<?> expr, Expression context) {
        return null;
    }

    @Override
    public Predicate visit(FactoryExpression<?> expr, Expression context) {
        return null;
    }

    @Override
    public Predicate visit(Operation<?> expr, Expression context) {
        String id = expr.getOperator().name();
        if (id.equals(Ops.AND.name())) {
            return Expressions.booleanOperation(Ops.OR,
                    expr.getArg(0).accept(this, null), expr.getArg(1).accept(this, null));
        } else {
            return (Predicate) expr;
        }
    }

    @Override
    public Predicate visit(ParamExpression<?> expr, Expression context) {
        return null;
    }

    @Override
    public Predicate visit(Path<?> expr, Expression context) {
        return null;
    }

    @Override
    public Predicate visit(SubQueryExpression<?> expr, Expression context) {
        return null;
    }

    @Override
    public Predicate visit(TemplateExpression<?> expr, Expression context) {
        return (Predicate)expr;
    }
}
