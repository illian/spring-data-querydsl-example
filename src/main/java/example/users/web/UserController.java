/*
 * Copyright 2015-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example.users.web;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import example.users.common.PhotoRepositoryAdd;
import example.users.entity.QAddress;
import example.users.entity.QPhoto;
import example.users.entity.QUser;
import example.users.entity.User;
import example.users.visitor.OrVisitor;
import example.users.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller to handle web requests for {@link User}s.
 *
 * @author Christoph Strobl
 * @author Oliver Gierke
 */
@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
class UserController {
    private final UserRepository repository;
    private final OrVisitor orVisitor = new OrVisitor();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    String index(Model model, //
                 @QuerydslPredicate(root = User.class) Predicate predicate,
                 @PageableDefault(sort = {"lastname", "firstname"}) Pageable pageable,
                 @RequestParam MultiValueMap<String, String> parameters) {
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
        builder.replaceQueryParam("page", new Object[0]);

        model.addAttribute("baseUri", builder.build().toUri());

        String joinsParam = parameters.getFirst("joins");
        Map<EntityPath, Path> joins;
        if (joinsParam != null && joinsParam.equals("false")) {
            joins = null;
        } else {
            joins = new HashMap<>(2);
            joins.put(QUser.user.address, QAddress.address);
            joins.put(QUser.user.photo, QPhoto.photo);
        }

        String orParam = parameters.getFirst("or");
        if (orParam != null && orParam.equals("on") && predicate != null) {
            predicate = predicate.accept(orVisitor, null);
        }

        Page<User> users = repository.getPage(predicate, pageable, joins);
        model.addAttribute("users", users);

        return "index";
    }
}
//http://localhost:8080/?firstname=cr&lastname=c&address.city=&address.street=&nationality=&or=on&languages=russian
