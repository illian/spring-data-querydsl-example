package example.users.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PHOTO")
public class Photo extends AbstractId {
    @Column(name = "LARGE")
    private String largePicture;

    @Column(name = "MEDIUM")
    private String mediumPicture;

    @Column(name = "SMALL")
    private String smallPicture;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User userPhoto;
}
