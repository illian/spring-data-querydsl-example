/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example.users.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import example.users.usertype.StringArrayUserType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;


/**
 * @author Christoph Strobl
 * @author Oliver Gierke
 */
@Getter
@Setter
@Entity(name = "USER")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "USER")
@TypeDefs({
        @TypeDef(name = "stringArray", typeClass = StringArrayUserType.class, parameters = {})
})
public class User extends AbstractId {
    @Column(name = "CREATED")
    @DateTimeFormat(pattern = "yyyy")
    private Date created;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "FIRSTNAME")
    private String firstname;

    @Column(name = "LASTNAME")
    private String lastname;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "NATIONALITY")
    private String nationality;

    @JsonIgnore
    @Column(name = "PASSWORD")
    private String password;

    @JsonUnwrapped
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "userAddress")
    private Address address;

    @JsonUnwrapped
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "userPhoto")
    private Photo photo;

    @Column(name = "IS_DELETED")
    private Boolean isDeleted;

    @Column(name = "IS_BLOCKED")
    private Boolean isBlocked;

    @Column(name = "LANGUAGES", columnDefinition = "ARRAY")
    @Type(type = "stringArray")
    private Object[] languages;
}
