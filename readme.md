# Spring Data Web - Querydsl example
This example is modified subproject from [Spring Data Examples](https://github.com/spring-projects/spring-data-examples "").

##What's changed
 - Database changed from **MongoDb** to relational **H2**. Db file placed in /data directory.
 - Address and Photo moved to separate entities.
 - Added fields **isBlocked** and **isDeleted**.

##What is presented
 - Work with Visitor - in received Predicate **AND** will replace on **OR**. See **OrVisitor**.
 - Work with Hibernate dialect extension.
 - Extending Spring data repository for work with Querydsl (single or all).
 - Work with few **Inner Join** in query. On demands query may be exetuted only once without requests to data in binded tables.